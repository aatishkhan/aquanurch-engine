import requests
import time
import datetime
from datetime import timezone


def data_request():
    """
    Fetches sensor mesaurements using the REST API and converts them into line protocol format. 

    Parameters
    ----------
    None

    Returns
    -------
    Nothing
    """

    # url of the REST API
    response = requests.get("http://18.190.121.60:8000/data/")
    data = response.json()
    t = data[0]['created_at']
    t = datetime.datetime.strptime(t, "%Y-%m-%d %H:%M:%S")
    # convert time to UNIX time format to be recognized by InfluxDB
    unix_ns = str(int(t.replace(tzinfo=timezone.utc).timestamp()))+"000000000"
    line = str(
        "sensor_reading"
        + " "
        + "pH="
        + str(data[0]["field1"])
        + ","
        + "Turbidity="
        + str(data[0]["field2"])
        + ","
        + "Temperature="
        + str(data[0]["field3"])
        + ","
        + "SoilMoisture="
        + str(data[0]["field4"])
        + " "
        + unix_ns
    )
    print(line)


if __name__ == "__main__":
    data_request()
