import ee
import pandas as pd
import geopandas as gpd
import math
from datetime import datetime
from datetime import timezone

ee.Initialize()

def download_met(geofile, start, end):
    """
    Retrieves  data from the ERA5-Land hourly model using Google Earth Engine https://developers.google.com/earth-engine/datasets/catalog/ECMWF_ERA5_LAND_HOURLY

    The standard units are converted to the units required by the GLM model.

    The output csv file is ready to be used for running the GLM model,    


    Parameters
    ----------
    geofile (str) -- path to the geojson file containing the name and coordinates of the site. 
      
    start (str) -- starting date of the data request in the form YYYY-MM-DD
    
    end (str) -- ending date areaof the data request in the form YYYY-MM-DD

    Returns
    -------
    Nothing

    The output csv file is saved in the current directory.


    """

    geo = create_geo(geofile)
    SatImage = ee.ImageCollection("ECMWF/ERA5_LAND/HOURLY").select(
        [
            "surface_solar_radiation_downwards",
            "surface_thermal_radiation_downwards",
            "temperature_2m",
            "dewpoint_temperature_2m",
            "u_component_of_wind_10m",
            "v_component_of_wind_10m",
            "total_precipitation_hourly",
            "snowfall",
        ]
    )
    SatImageTime = SatImage.filterDate(start, end)  # YYYY-MM-DD
    info = SatImageTime.filterBounds(geo).getRegion(geo, 500).getInfo()
    info_dict = [dict(zip(info[0], values)) for values in info[1:]]
    df = pd.DataFrame(info_dict)
    df["time"] = df["time"].apply(unixto_utc)
    df["time"] = df["time"].astype("datetime64[ns]")
    df["surface_solar_radiation_downwards"] = df.apply(
        lambda x: convertto_wm(x["time"], x["surface_solar_radiation_downwards"]),
        axis=1,
    )
    df["surface_thermal_radiation_downwards"] = df.apply(
        lambda x: convertto_wm(x["time"], x["surface_thermal_radiation_downwards"]),
        axis=1,
    )
    df["temperature_2m"] = df["temperature_2m"].apply(converto_c)
    df["dewpoint_temperature_2m"] = df["dewpoint_temperature_2m"].apply(converto_c)
    df["RelHum"] = df.apply(
        lambda x: calculate_rh(x["temperature_2m"], x["dewpoint_temperature_2m"]),
        axis=1,
    )
    df["WindSpeed"] = df.apply(
        lambda x: calculate_windspeed(
            x["u_component_of_wind_10m"], x["v_component_of_wind_10m"]
        ),
        axis=1,
    )
    df.rename(
        columns={
            "time": "Date",
            "surface_solar_radiation_downwards": "ShortWave",
            "surface_thermal_radiation_downwards": "LongWave",
            "temperature_2m": "AirTemp",
            "total_precipitation_hourly": "Rain",
            "snowfall": "Snow",
        },
        inplace=True,
    )
    new_df = df[
        [
            "Date",
            "ShortWave",
            "LongWave",
            "AirTemp",
            "RelHum",
            "WindSpeed",
            "Rain",
            "Snow",
        ]
    ]
    new_df.to_csv("meteo.csv", index=False)


def create_geo(geofile):
    """
    creates a GEE geometry from the input file
    Parameters
    ----------
    geofile (str) -- path to the file containing the name and coordinates of ROI, currently tested with geojson. 
    Returns
    -------
    geo -- object of ee.Geometry type
    """
    df = gpd.read_file(geofile)
    if (df.geometry.type == "Point").bool():
        # extract coordinates
        lon = float(df.geometry.x)
        lat = float(df.geometry.y)
        # create geometry
        geo = ee.Geometry.Point(lon, lat)

    elif (df.geometry.type == "Polygon").bool():
        # extract coordinates
        area = [
            list(df.geometry.exterior[row_id].coords) for row_id in range(df.shape[0])
        ]
        # create geometry
        for i in range(len(area[0])):
            area[0][i] = area[0][i][0:2]
        geo = ee.Geometry.Polygon(area)

    else:
        # if the input geometry type is not
        raise ValueError("geometry type not supported")

    return geo


def unixto_utc(timestamp):
    timestamp = str(timestamp)
    timestamp = timestamp[:-3]
    timestamp = int(timestamp)
    dt_object = datetime.fromtimestamp(timestamp, tz=timezone.utc)
    return dt_object.strftime("%Y-%m-%d %H:%M:%S")


def convertto_wm(df_time, val):
    df_time = df_time.to_pydatetime()
    df_time = int(datetime.strftime(df_time, "%H"))
    if df_time == 0:
        df_time = 1
    return val / (df_time * 60 * 60)


def converto_c(temp):
    return temp - 273.15


def calculate_rh(temp, dewpoint):
    """
    https://en.wikipedia.org/wiki/Dew_point?oldformat=true
    https://earthscience.stackexchange.com/questions/16570/how-to-calculate-relative-humidity-from-temperature-dew-point-and-pressure
    """
    rh = 100 - (5 * (temp - dewpoint))
    return rh


def calculate_windspeed(u_comp, v_comp):
    ws = math.sqrt((u_comp * u_comp) + (v_comp * v_comp))
    return ws
