from django.db import models

# Create your models here.
class data(models.Model):
    deviceId=models.CharField(max_length=30)
    metaData=models.CharField(max_length=30)
    description=models.CharField(max_length=30)
    createdAt=models.CharField(max_length=30)

    def __str__(self):
        return self.deviceId