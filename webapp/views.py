from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from . models import data
from . serializers import dataSerializer

class DataList(APIView):
    
    def get(self, request):
        aqua_data = data.objects.all()
        serializer = dataSerializer(aqua_data, many=True)
        return Response(serializer.data)
    
    def post(self, request):
        return True

    def put(self, request):
        return True

    def delete(self, request):
        return True